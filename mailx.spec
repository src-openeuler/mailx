Name:           mailx
Version:        12.5
Release:        34
License:        BSD with advertising and MPLv1.1
Summary:        Enhanced implementation of the mailx command
URL:            http://heirloom.sourceforge.net/mailx.html
Source0:        http://ftp.debian.org/debian/pool/main/h/heirloom-mailx/heirloom-mailx_12.5.orig.tar.gz
Source1:        get-upstream-tarball.sh

Patch0:         nail-11.25-config.patch
Patch1:         mailx-12.3-pager.patch
Patch2:         mailx-12.5-lzw.patch
Patch3:         mailx-12.5-fname-null.patch
Patch4:         mailx-12.5-collect.patch
Patch5:         mailx-12.5-usage.patch
Patch6:         mailx-12.5-man-page-fixes.patch
#From: Florian Weimer <fweimer@redhat.com>
Patch7:         mailx-12.5-outof-Introduce-expandaddr-flag.patch
#From: Florian Weimer <fweimer@redhat.com>
Patch8:         mailx-12.5-fio.c-Unconditionally-require-wordexp-support.patch
#From: Florian Weimer <fweimer@redhat.com>
Patch9:         mailx-12.5-globname-Invoke-wordexp-with-WRDE_NOCMD-CVE-2004-277.patch
#From: Florian Weimer <fweimer@redhat.com>
Patch10:        mailx-12.5-unpack-Disable-option-processing-for-email-addresses.patch
Patch11:        mailx-12.5-empty-from.patch
#From: =?UTF-8?q?Nikola=20Forr=C3=B3?= <nforro@redhat.com>
Patch12:        mailx-12.5-nss-hostname-matching.patch
Patch13:        mailx-12.5-encsplit.patch
Patch14:        mailx-12.5-openssl.patch
Patch15:        backport-mailx-12.5-no-SSLv3.patch
Patch9000:      set-mail-defualt-did-not-record-failsend.patch

BuildRequires:  gcc openssl-devel 

Obsoletes:      nail < %{version}
Provides:       nail = %{version} /bin/mail /bin/mailx

%description
Heirloom mailx (previously known as nail) is a mail user agent for Unix systems.

%package_help

%prep
%autosetup -n heirloom-%{name}-%{version} -p1

sed -i 's,/etc/nail.rc,%{_sysconfdir}/mail.rc,g' mailx.1

%build
export LDFLAGS="%{build_ldflags}"

echo    PREFIX=%{_prefix} \
    BINDIR=%{_bindir} \
    MANDIR=%{_mandir} \
    SYSCONFDIR=%{_sysconfdir} \
    MAILRC=%{_sysconfdir}/mail.rc \
    MAILSPOOL=%{_localstatedir}/mail \
    SENDMAIL=%{_sbindir}/sendmail \
    UCBINSTALL=install \
> makeflags

make `cat makeflags`  CFLAGS="$RPM_OPT_FLAGS -D_GNU_SOURCE -D_FILE_OFFSET_BITS=64"  IPv6=-DHAVE_IPv6_FUNCS

%install
%make_install STRIP=: `cat makeflags`

pushd $RPM_BUILD_ROOT%{_bindir}
ln -s mailx mail
ln -s mailx Mail
ln -s mailx nail
popd

pushd $RPM_BUILD_ROOT%{_mandir}/man1
ln -s mailx.1 mail.1
ln -s mailx.1 Mail.1
ln -s mailx.1 nail.1
popd

%triggerpostun -- mailx < 12
[[ -f %{_sysconfdir}/mail.rc.rpmnew ]] && {
    ( echo '# The settings above was inherited from the old mailx-8.x config'
      echo
      cat %{_sysconfdir}/mail.rc.rpmnew
    ) >>%{_sysconfdir}/mail.rc
} || :


%triggerpostun -- nail <= 12.3
[[ -f %{_sysconfdir}/nail.rc.rpmsave ]] && {
    save=%{_sysconfdir}/mail.rc.rpmnew
    [[ -f $save ]] && save=%{_sysconfdir}/mail.rc.rpmsave

    mv -f %{_sysconfdir}/mail.rc $save
    mv -f %{_sysconfdir}/nail.rc.rpmsave %{_sysconfdir}/mail.rc
} || :


%files
%defattr(-,root,root)
%doc COPYING AUTHORS
%config(noreplace) %{_sysconfdir}/mail.rc
%{_bindir}/*

%files help
%defattr(-,root,root)
%doc README
%{_mandir}/*/*


%changelog
* Thu Feb 02 2023 gaihuiying <eaglegai@163.com> - 12.5-34
- DESC:fix build with openssl 3.0

* Fri Jul 30 2021 chenyanpanHW <chenyanpan@huawei.com> - 12.5-33
- DESC: delete -S git from %autosetup, and delete BuildRequires git

* Tue Dec 31 2019 openEuler Buildteam <buildteam@openeuler.org> - 12.5-32
- Type:bugfix
- Id:NA
- SUG:NA
- DESC:optimization the spec

* Mon Sep 16 2019 openEuler Buildteam <buildteam@openeuler.org> - 12.5-31
- Package init
